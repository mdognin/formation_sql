--Liste des communes où le taux d’évolution des ventes est supérieur à 20 % entre le premier et le second trimestre de 2020.

CREATE OR REPLACE VIEW v_vente --On crée une vue avec la préselection des biens concernés par la requete
AS SELECT commune.nom_commune, mutation.valeur_fonciere AS valeur, bien.surface_lot_1 AS surface, EXTRACT(QUARTER FROM Mutation.date_mutation) AS tri
	FROM Bien
	INNER JOIN Adresse ON adresse.id_adresse=bien.adresse_id
	INNER JOIN Commune ON commune.id_commune=adresse.commune_id
	INNER JOIN Mutation ON Bien.id_bien = Mutation.bien_id
	WHERE Mutation.nature_mutation LIKE 'Vente' 
		AND mutation.valeur_fonciere > 0 
		AND EXTRACT(QUARTER FROM Mutation.date_mutation) IN (1,2) 
		AND EXTRACT(YEAR FROM Mutation.date_mutation) = 2020;

WITH w_vente_t1
AS ( --On compte les vente de chaque commune au T1
	SELECT nom_commune, COUNT(*) AS vente_tri1
	FROM v_vente
	WHERE tri=1
	GROUP BY v_vente.nom_commune ),
	w_vente_t2 AS ( --On compte les vente de chaque commune au T2
	SELECT nom_commune, COUNT(*) AS vente_tri2
	FROM v_vente
	WHERE tri=2
	GROUP BY nom_commune ),
	w_taux AS ( --On calcule le taux
	SELECT w_vente_t1.nom_commune, (vente_tri2-vente_tri1)*100/vente_tri1 AS taux
	FROM w_vente_t1,w_vente_t2
	WHERE w_vente_t2.nom_commune = w_vente_t1.nom_commune)

SELECT w_taux.nom_commune,w_taux.taux, vente_tri1,vente_tri2
FROM w_vente_t1
INNER JOIN w_vente_t2 ON w_vente_t2.nom_commune = w_vente_t1.nom_commune
INNER JOIN w_taux ON w_taux.nom_commune = w_vente_t1.nom_commune
AND taux >=20
ORDER BY nom_commune ASC;
