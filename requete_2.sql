--Proportion des ventes d’appartements par le nombre de pièces.

--Version 3
SELECT Bien.nbr_piece AS "Nbr de pièces",COUNT(*) AS "Nbr de biens",ROUND(COUNT(*)*100/
	(
	SELECT COUNT(*) 
	FROM Bien
	INNER JOIN Mutation ON Bien.id_bien = Mutation.bien_id
	WHERE Mutation.nature_mutation LIKE 'Vente' 
		AND Bien.type_local LIKE 'Appartement'
	),2) AS "Pourcentage"
FROM Bien
GROUP BY Bien.nbr_piece ORDER BY Bien.nbr_piece ASC;
