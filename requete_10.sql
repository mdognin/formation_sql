--Donnez les moyennes de valeurs foncières pour le top 20 des communes

CREATE OR REPLACE VIEW v_temp1
AS
	SELECT commune.nom_commune AS nom_commune, ROUND(AVG(mutation.valeur_fonciere),2) AS val_fonciere_moy_commune 
	FROM bien
	INNER JOIN mutation ON mutation.bien_id=bien.id_bien
	INNER JOIN adresse ON adresse.id_adresse=bien.adresse_id
	INNER JOIN commune ON commune.id_commune=adresse.commune_id
	WHERE mutation.valeur_fonciere >0
	GROUP BY commune.nom_commune;

SELECT v_temp1.nom_commune,  v_temp1.val_fonciere_moy_commune
FROM v_temp1
ORDER BY val_fonciere_moy_commune DESC LIMIT 20;
