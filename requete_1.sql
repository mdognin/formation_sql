--Nombre total d’appartements vendus au 1er semestre 2020.

SELECT COUNT(*) AS nbr_appart_vendu_S1
FROM Bien
INNER JOIN Mutation ON Bien.id_bien = Mutation.bien_id
WHERE Mutation.nature_mutation LIKE 'Vente' 
	AND Bien.type_local LIKE 'Appartement'
	AND EXTRACT(MONTH FROM Mutation.date_mutation)<7 
	AND EXTRACT(YEAR FROM Mutation.date_mutation) = 2020;
	
