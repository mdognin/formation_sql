--Liste des 10 départements où le prix du mètre carré est le plus élevé


--etape 4
SELECT ROUND(AVG(mutation.valeur_fonciere/bien.surface_lot_1),2) AS "Prix_moyen_m_carre", commune.code_departement 
FROM bien
INNER JOIN mutation ON mutation.bien_id=bien.id_bien
INNER JOIN adresse ON adresse.id_adresse=bien.adresse_id
INNER JOIN commune ON commune.id_commune=adresse.commune_id
WHERE mutation.valeur_fonciere >0
GROUP BY commune.code_departement ORDER BY "Prix_moyen_m_carre" DESC LIMIT 10;
