--Taux d’appartements qui ont été vendus à un prix du mètre carré deux fois plus élevé que le prix du mètre carré moyen du département

CREATE OR REPLACE VIEW V_r9
AS SELECT mutation.valeur_fonciere AS valeur, bien.surface_lot_1 AS surface,commune.code_departement
	FROM bien
	INNER JOIN mutation ON mutation.bien_id=bien.id_bien
	INNER JOIN adresse ON adresse.id_adresse=bien.adresse_id
	INNER JOIN commune ON commune.id_commune=adresse.commune_id
	WHERE bien.type_local LIKE 'Appartement' AND mutation.valeur_fonciere >0;

WITH	w_val_moy_dep AS (		--On fait le calcul de la moyenne de vente d'un appartement au m2 par département
	SELECT V_r9.code_departement, ROUND(AVG(valeur/surface),2) AS prix_moyen_m_carre 
	FROM V_r9
	GROUP BY V_r9.code_departement),
		w_nbr_appart AS (		--Ensuite on calcule par département combien d'appartemnt on été vendu à 2 * la moyenne précédemment calculée
	SELECT V_r9.code_departement, COUNT(*) AS vente_2_fois_moy
	FROM V_r9
	INNER JOIN w_val_moy_dep ON w_val_moy_dep.code_departement = V_r9.code_departement
	WHERE valeur/surface > w_val_moy_dep.prix_moyen_m_carre*2
	GROUP BY V_r9.code_departement ),
		w_total AS (			--Enfin on on calcule le nombre d'appartement vendu par département
	SELECT V_r9.code_departement, COUNT(*) AS total_vente
	FROM V_r9
	GROUP BY V_r9.code_departement )

								--On calcule le taux dans le résultat final
SELECT w_nbr_appart.code_departement,total_vente,vente_2_fois_moy,vente_2_fois_moy*100/total_vente AS taux,prix_moyen_m_carre 
FROM w_nbr_appart
INNER JOIN w_total ON w_nbr_appart.code_departement = w_total.code_departement
INNER JOIN w_val_moy_dep ON  w_nbr_appart.code_departement = w_val_moy_dep.code_departement;
