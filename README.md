# Projet SQL : Réalisation de 10 requêtes SQL pour extraire certaines informations d'une base de donnée de ventes de biens immobiliers

Source de données : https://www.data.gouv.fr/fr/datasets/5c4ae55a634f4117716d5656/

Présentation des résultats des requêtes dans le powerpoint

- R1 : Nombre total d’appartements vendus au 1er semestre 2020
- R2 : Proportion des ventes d’appartements par le nombre de pièces
- R3 : Liste des 10 départements où le prix du mètre carré est le plus élevé
- R4 : Prix moyen du mètre carré d’une maison en Île-de-France
- R5 : LIste des 10 appartements les plus chers avec departement et nbr de m2
- R6 : Taux d’évolution du nombre de ventes entre le premier et le deuxième trimestres de 2020
- R7 : Liste des communes où le taux d’évolution des ventes est supérieur à 20 % entre le premier et le second trimestre de 2020
- R8 : Différence en pourcentage du prix au mètre carré entre un appartement de 2 pièces et un appartement de 3 pièces
- R9 : Taux d’appartements qui ont été vendus à un prix du mètre carré deux fois plus élevé que le prix du mètre carré moyen du département
- R10 : Donnez les moyennes de valeurs foncières pour le top 20 des communes
