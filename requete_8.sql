--Différence en pourcentage du prix au mètre carré entre un appartement de 2 pièces et un appartement de 3 pièces

CREATE OR REPLACE VIEW Appart 	--Création de la vue pour extraire le prix moyen du m carré pour un 2 pièce et un 3 pièce
AS
	SELECT Bien.nbr_piece, ROUND(AVG(Mutation.valeur_fonciere/Bien.surface_lot_1),2) AS val_moy
	FROM Bien
	INNER JOIN Mutation ON Mutation.bien_id = Bien.ID_BIEN
	WHERE Bien. TYPE_LOCAL LIKE 'Appartement' 
	AND Mutation.VALEUR_FONCIERE > 0 
	AND Bien.nbr_piece IN (2 ,3 )
	GROUP BY Bien.nbr_piece;

WITH 	W_val_2p AS ( 	--Utilisation de WITH pour extraire les valeur de la vue dans un tableau et faire le calcul
	SELECT val_moy AS val_2p
	FROM Appart
	WHERE Appart.nbr_piece = 2),
		W_val_3p AS (
	SELECT val_moy AS val_3p
	FROM Appart
	WHERE Appart.nbr_piece = 3), 
		W_diff AS (
	SELECT ROUND((val_3p-val_2p)/val_2p*100,2) AS diff_pourcent
	FROM W_val_2p, W_val_3p)
  
	SELECT *
	FROM W_val_2p, W_val_3p, W_diff;
