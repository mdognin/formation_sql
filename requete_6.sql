--Taux d’évolution du nombre de ventes entre le premier et le deuxième trimestres de 2020

--AVEC WITH
WITH 	nbr_vente_t1 AS (SELECT COUNT(*) AS nbr_vente_t1 
FROM Mutation
			WHERE EXTRACT(QUARTER FROM Mutation.date_mutation)=1 
			AND EXTRACT(YEAR FROM Mutation.date_mutation) = 2020),
			
		nbr_vente_t2 AS (SELECT COUNT(*) AS nbr_vente_t2 
		FROM Mutation
			WHERE EXTRACT(QUARTER FROM Mutation.date_mutation)=2 
			AND EXTRACT(YEAR FROM Mutation.date_mutation) = 2020),
			
		taux AS (SELECT (nbr_vente_t2-nbr_vente_t1)*100/nbr_vente_t1 AS taux 
		FROM nbr_vente_t1,nbr_vente_t2)
		
SELECT * 
FROM nbr_vente_t1, nbr_vente_t2, taux;

--avec fonction
DROP TABLE T_nbr_vente;
CREATE TEMPORARY TABLE T_nbr_vente(
	nbr_vente_Q_1 INT,
	nbr_vente_Q_2 INT,
	taux_pourcent INT);
CREATE OR REPLACE FUNCTION f_nbr_vente (IN trimestre int,IN annee int)
RETURNS integer AS $temp1$
	DECLARE
		temp1 integer;
	BEGIN
			SELECT COUNT(*) INTO temp1 FROM Mutation
			WHERE EXTRACT(QUARTER FROM Mutation.date_mutation)=trimestre AND EXTRACT(YEAR FROM Mutation.date_mutation) = annee;
			RETURN temp1;			
	END;
$temp1$ LANGUAGE plpgsql;
				--AU lieu de récrire plusieurs fois les mêmes fonctions, comment stocker leur résultat dans une variables? 
INSERT INTO T_nbr_vente SELECT f_nbr_vente(1,2020),f_nbr_vente(2,2020),(f_nbr_vente(2,2020)-f_nbr_vente(1,2020))*100/f_nbr_vente(1,2020);
SELECT * FROM T_nbr_vente;
