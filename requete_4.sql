--Prix moyen du mètre carré d’une maison en Île-de-France.


SELECT ROUND(AVG(mutation.valeur_fonciere/bien.surface_lot_1),2) AS prix_moyen_m_carre 
FROM bien
INNER JOIN mutation ON mutation.bien_id=bien.id_bien
INNER JOIN adresse ON adresse.id_adresse=bien.adresse_id
INNER JOIN commune ON commune.id_commune=adresse.commune_id
WHERE bien.type_local LIKE 'Maison' AND mutation.valeur_fonciere >0 
	AND commune.code_departement IN('75','77','78','91','92','93','94','95');
