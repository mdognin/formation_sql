-- LIste des 10 appartements les plus chers avec departement et nbr de m2

SELECT Bien.surface_lot_1, commune.code_departement, mutation.valeur_fonciere, bien.id_bien 
FROM Bien

INNER JOIN Mutation ON Bien.id_bien = Mutation.bien_id
INNER JOIN adresse ON adresse.id_adresse=bien.adresse_id
INNER JOIN commune ON commune.id_commune=adresse.commune_id

WHERE Mutation.nature_mutation LIKE 'Vente' 
	AND Bien.type_local LIKE 'Appartement' 
	AND mutation.valeur_fonciere >0

ORDER BY mutation.valeur_fonciere DESC LIMIT 10;
